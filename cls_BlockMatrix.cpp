//
// Created by puhovity on 25.07.16.
//

#include "cls_BlockMatrix.h"


cls_BlockMatrix::cls_BlockMatrix() {
    m_blocksCount = 3;
    m_blockSize = 3;
    m_Composer.setSize(m_blocksCount*m_blockSize);
}

cls_BlockMatrix::cls_BlockMatrix(int blocksCount, int blockSize) {
    m_blocksCount = blocksCount;
    m_blockSize = blockSize;
    m_Composer.setSize(m_blocksCount*m_blockSize);
}

// отсылка блока на позицию (iPos, jPos), нумерация с нуля
int cls_BlockMatrix::sendBlock(cls_SingleBlock& singleBlock, int iPos, int jPos) {
    int iOffset = iPos * m_blockSize;
    int jOffset = jPos * m_blockSize;
    int blockSize = singleBlock.getSize();

    for(int i = 0; i < blockSize; i++) {
        for(int j = 0; j < blockSize; j++) {
            if(singleBlock.getValue(i, j) != 0) {
                m_Composer.setValue(iOffset + i, jOffset + j, singleBlock.getValue(i, j));
            }
        }
    }

    return 0;
}

int cls_BlockMatrix::getBlocksCount() {
    return m_blocksCount;
}


int cls_BlockMatrix::getBlockSize() {
    return m_blockSize;
}

float cls_BlockMatrix::getValue(int i, int j) {
    return m_Composer.getValue(i, j);
}

int cls_BlockMatrix::setValue(int i, int j, float val) {
    return m_Composer.setValue(i, j, val);
}

cls_Composer &cls_BlockMatrix::getComposer() {
    return m_Composer;
}

int cls_BlockMatrix::fill() {
    return 0;
}
