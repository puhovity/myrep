//
// Created by puhovity on 02.08.16.
//

#include "cls_CSLR.h"
#include "cls_BlockMatrix.h"
cls_CSLR::cls_CSLR(cls_BlockMatrix *inputMatrix) {


    // размер одной стороны матрицы
    m_matrixSize = inputMatrix->getBlocksCount() * inputMatrix->getBlockSize();

    cls_Composer inputMatrixComposer = inputMatrix->getComposer();

    // количество элементов матрицы, которым были заданы значения
    int elementsCount = inputMatrixComposer.getLTStoredElementsCount();

    m_adiag = new float[m_matrixSize];
    m_jptr = new int[elementsCount];
    m_iptr = new int[m_matrixSize + 1];
    m_altr = new float[elementsCount];
    m_autr = new float[elementsCount];

    // заполнение диагонали
    for(int i = 0; i < m_matrixSize; i++) {
        m_adiag[i] = inputMatrix->getValue(i, i);
    }

    // заполнение altr, autr, iptr, jptr
    int nextElemNumber = 0; // номер следующего ненулевого элемента
    int curr_row = 0;
    for( std::vector<std::map<int, float>>::iterator it_i = inputMatrixComposer.getStruc().begin();
         it_i != inputMatrixComposer.getStruc().end();
         it_i++ ) {

        // это корректно, потому что 1. матрица имеет ненулевую диагональ, значит it_i проходит все строки
        // 2. nextElemNumber это номер, который имеет следующий ненулевой элемент, а если в строке нет ненулевых элементов,
        // то nextElemNumber останется таким же, а это значит, что i_ptr[i+1] - i_ptr[i] = 0
        curr_row = it_i - inputMatrixComposer.getStruc().begin();
        m_iptr[curr_row] = nextElemNumber;
        for ( std::map<int, float>::iterator it_j = it_i->begin();
              it_j != it_i->end();
              it_j++ ) {

            int curr_col = it_j->first;
            // рассматриваем только нижний треугольник
            if( curr_col >= curr_row ) {
                break;
            }

            m_altr[nextElemNumber] = it_j->second;
            m_autr[nextElemNumber] = inputMatrixComposer.getValue(curr_col, curr_row);
            m_jptr[nextElemNumber] = curr_col;
            nextElemNumber++;
        }
    }

    m_iptr[curr_row+1] = elementsCount;

}

cls_CSLR::~cls_CSLR() {
    delete[] m_adiag;
    delete[] m_jptr;
    delete[] m_iptr;
    delete[] m_altr;
    delete[] m_autr;
}

// z = Ax
cls_Vector cls_CSLR::operator*(cls_Vector& x) {

    cls_Vector z(x.getSize());
    for(int i = 0; i < x.getSize(); i++){
        z.setValue(i, x.getValue(i) * m_adiag[i]);
    }

    for(int i = 0; i < x.getSize(); i++) {
        for(int j = m_iptr[i]; j < m_iptr[i+1]; j++) {
            z.setValue(i, z.getValue(i) + x.getValue(m_jptr[j]) * m_altr[j]);
            z.setValue(m_jptr[j], z.getValue(m_jptr[j]) + x.getValue(i) * m_autr[j]);
        }
    }

    return z;
}
