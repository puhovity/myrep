//
// Created by puhovity on 25.07.16.
//

#ifndef MYREP_CLS_BLOCKMATRIX_H
#define MYREP_CLS_BLOCKMATRIX_H
#include "cls_Composer.h"
#include "cls_SingleBlock.h"

class cls_BlockMatrix {
protected:
    cls_Composer m_Composer;
    int m_blocksCount;
    int m_blockSize;

public:
    cls_BlockMatrix();
    cls_BlockMatrix(int blocksCount, int blockSize);

    int getBlocksCount();
    int getBlockSize();

    int setValue(int i, int j, float val);
    float getValue(int i, int j);

    cls_Composer& getComposer();


    // виртуальный метод fill для заполнения (используется, например, для радномной генерации)
    int virtual fill();

    // вставить блок размера m_blockSize в iPos, jPos - позиция блока singleBlock в блочной матрице
    int sendBlock(cls_SingleBlock& singleBlock, int iPos, int jPos);
};


#endif //MYREP_CLS_BLOCKMATRIX_H
