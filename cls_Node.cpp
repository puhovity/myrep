//
// Created by puhovity on 10.08.16.
//

#include "cls_Node.h"
cls_Node::cls_Node() {
    m_Id = -1;
}

cls_Node::cls_Node(int id, double x, double y, double z) {
    m_Id = id;
    m_Coords.push_back(x);
    m_Coords.push_back(y);
    m_Coords.push_back(z);
}


cls_Node::~cls_Node() {

}