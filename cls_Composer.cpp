//
// Created by puhovity on 25.07.16.
//

#include "cls_Composer.h"

int cls_Composer::setValue(int row, int col, float val) {
    struc[row][col] = val;
}

int cls_Composer::getValue(int row, int col) {
    if ( isset(row, col) ) {
        return struc[row][col];
    }

    return 0;
}

int cls_Composer::getStoredElementsCount() {
    int elements_count = 0;

    for (std::vector<std::map<int, float>>::iterator it = struc.begin(); it != struc.end(); ++it) {
        elements_count += it->size();
    }

    return elements_count;
}

std::vector<std::map<int, float>> &cls_Composer::getStruc() {
    return struc;
}

bool cls_Composer::isset(int row, int col) {
    if (struc[row].count(col) > 0) {
        return true;
    }
    return false;
}

cls_Composer::cls_Composer() {
    setSize(3);
}

cls_Composer::cls_Composer(int size) {
    setSize(size);
}

int cls_Composer::setSize(int size) {
    m_size = size;
    struc.resize(size);
}

int cls_Composer::getLTStoredElementsCount() {
    int elements_count = 0;

    for (std::vector<std::map<int, float>>::iterator it_i = struc.begin(); it_i != struc.end(); ++it_i) {
        int curr_row = it_i - struc.begin();
        for (std::map<int, float>::iterator it_j = it_i->begin();
             it_j != it_i->end();
             it_j++) {

            int curr_col = it_j->first;
            // рассматриваем только нижний треугольник
            if (curr_col >= curr_row) {
                break;
            }
            elements_count++;

        }
    }

    return elements_count;
}
