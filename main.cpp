#include <iostream>
#include "cls_CSLR.h"
#include "cls_RandBlockMatrixSymFace.h"
#include "cls_BCGSolver.h"
#include "lib_print.h"
using namespace std;

int main() {

    // рандомизация
    srand(time(0));

    cls_RandBlockMatrixSymFace bm(2, 2);
    bm.fill();

    cls_CSLR A(&bm);

    std::cout << "Матрица A" << std::endl;
    printBlockMatrix(bm);

    cls_Vector b(4);
    b.setValue(0, 1.3);
    b.setValue(1, 2.5);
    b.setValue(2, 3);
    b.setValue(3, 4);

    cls_BCGSolver solver;
    cls_Vector x = solver.solve(A, b, 0.1);

    std::cout << "Вектор правой части" << std::endl;
    printVector(b);

    std::cout << "Найденный вектор x" << std::endl;
    printVector(x);

    std::cout << "Результат умножения матрицы A на вектор x" << std::endl;
    printVector(A*x);
    return 0;
}