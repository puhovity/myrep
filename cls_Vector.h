//
// Created by puhovity on 03.08.16.
//

#ifndef MYREP_CLS_VECTOR_H
#define MYREP_CLS_VECTOR_H


class cls_Vector {
protected:
    int m_size;
    double* m_elem;
public:
    cls_Vector();
    cls_Vector(int size);
    cls_Vector(const cls_Vector& vec);
    ~cls_Vector();
    int setValue(int pos, double val);
    double getValue(int pos) const;
    int getSize() const;

    double operator*(cls_Vector vec) const;
    cls_Vector operator-(cls_Vector vec) const;
    cls_Vector operator+(cls_Vector vec) const;
    cls_Vector operator*(double alpha);
    cls_Vector& operator=(cls_Vector vec);
};


#endif //MYREP_CLS_VECTOR_H
