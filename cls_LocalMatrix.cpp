//
// Created by puhovity on 10.08.16.
//

#include "cls_LocalMatrix.h"

cls_LocalMatrix::cls_LocalMatrix() {
    m_NodesCount = 4;
    m_Blocks.resize(m_NodesCount * m_NodesCount);
}

cls_LocalMatrix::cls_LocalMatrix(int nodesCount) {
    m_NodesCount = nodesCount;
    m_Blocks.resize(m_NodesCount*m_NodesCount);
}

void cls_LocalMatrix::appendBlock(cls_SingleBlock &sBlock, int iPos, int jPos) {
    m_Blocks[iPos * 3 + jPos] = sBlock;

    std::pair<unsigned, unsigned> nodesIds(m_NodesIds[iPos], m_NodesIds[jPos]);
    m_Blocks[iPos * 3 + jPos].setNodesIds(nodesIds);
}

// возвращаем блок + указываем в нем информацию, к каким глобальным узлам он относится
cls_SingleBlock cls_LocalMatrix::getBlock(int iPos, int jPos) {
    return m_Blocks[iPos * 3 + jPos];
}

cls_LocalMatrix::iterator cls_LocalMatrix::begin() {
    return m_Blocks.begin();
}


cls_LocalMatrix::iterator cls_LocalMatrix::end() {
    return m_Blocks.end();
}
