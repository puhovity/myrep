//
// Created by puhovity on 10.08.16.
//

#ifndef MYREP_CLS_LOCALMATRIX_H
#define MYREP_CLS_LOCALMATRIX_H

#include <vector>
#include "cls_SingleBlock.h"

class cls_LocalMatrix {


private:
    int m_NodesCount;
    std::vector<cls_SingleBlock> m_Blocks;
    std::vector<unsigned> m_NodesIds;

public:
    typedef std::vector<cls_SingleBlock>::iterator iterator;

    cls_LocalMatrix();
    cls_LocalMatrix(int nodesCount);

    void appendBlock(cls_SingleBlock& sBlock, int iPos, int jPos);
    cls_SingleBlock getBlock(int iPos, int jPos);

    iterator begin();
    iterator end();
};


#endif //MYREP_CLS_LOCALMATRIX_H
