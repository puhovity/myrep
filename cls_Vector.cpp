//
// Created by puhovity on 03.08.16.
//

#include <new>
#include "cls_Vector.h"

cls_Vector::cls_Vector() {
    m_size = 4;
    try {
        m_elem = new double[m_size];
        for(int i = 0; i < m_size; i++) {
            m_elem[i] = 0;
        }
    }   catch(std::bad_alloc &ba) {
        // обработка исключения, возможно здесь будет логгер
    }
}

cls_Vector::cls_Vector(int size) {
    m_size = size;
    try {
        m_elem = new double[m_size];
        for(int i = 0; i < m_size; i++) {
            m_elem[i] = 0;
        }
    }   catch(std::bad_alloc &ba) {
        // обработка исключения, возможно здесь будет логгер
    }
}


cls_Vector::cls_Vector(const cls_Vector& vec) {
    m_size = vec.getSize();
    try {
        m_elem = new double[m_size];
        for(int i = 0; i < m_size; i++) {
            m_elem[i] = vec.getValue(i);
        }
    }   catch(std::bad_alloc &ba) {
        // обработка исключения, возможно здесь будет логгер
    }
}

cls_Vector::~cls_Vector() {
    delete[] m_elem;
}


int cls_Vector::setValue(int pos, double val) {
    if(pos >= m_size) {
        return 1;
    }

    m_elem[pos] = val;
}


double cls_Vector::getValue(int pos) const {
    if(pos >= m_size) {
        return 0;
    }

    return m_elem[pos];
}

int cls_Vector::getSize() const {
    return m_size;
}


double cls_Vector::operator*(cls_Vector vec) const{
    double scalarProduct = 0;
    for(int i = 0; i < m_size; i++ ) {
        scalarProduct += m_elem[i] * vec.getValue(i);
    }

    return scalarProduct;
}

cls_Vector cls_Vector::operator-(cls_Vector vec) const {
    cls_Vector res = *this;
    for(int i = 0; i < m_size; i++) {
        res.setValue(i, res.getValue(i) - vec.getValue(i));
    }

    return res;
}

cls_Vector cls_Vector::operator+(cls_Vector vec) const {
    cls_Vector res = *this;
    for(int i = 0; i < m_size; i++) {
        res.setValue(i, res.getValue(i) + vec.getValue(i));
    }

    return res;
}

cls_Vector cls_Vector::operator*(double alpha) {
    cls_Vector res = *this;
    for(int i = 0; i < m_size; i++) {
        res.setValue(i, res.getValue(i) * alpha);
    }

    return res;
}

cls_Vector& cls_Vector::operator=(cls_Vector vec) {
    delete[] m_elem;
    m_size = vec.getSize();
    m_elem = new double[m_size];
    for(int i = 0; i < m_size; i++) {
        m_elem[i] = vec.getValue(i);
    }

    return *this;
}
