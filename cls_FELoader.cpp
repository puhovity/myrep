//
// Created by puhovity on 16.08.16.
//

#include <fstream>
#include <iostream>
#include "cls_FELoader.h"
#include "cls_RandBlockMatrixSymFace.h"

int cls_FELoader::loadFromFile(std::string path) {
    std::ifstream infile(path);

    if( !infile ) {
        return 1;
    }

    // считываем первую строку
    int nodesCount, dimensionSize;
    infile >> nodesCount >> dimensionSize;

    m_NodesCount = nodesCount;

    std::cout << nodesCount << "\n" << dimensionSize << "\n";

    for(int i = 0; i < nodesCount; i++ ) {
        double x, y, z;
        infile >> x >> y >> z;
        cls_Node newNode(i, x, y, z);
        m_Nodes.push_back(newNode);

        // для проверки выведем последний элемент
        if ( i == nodesCount - 1 ) {
            std::cout << x <<  "\n" << y << "\n" << z << "\n";
        }
    }

    int FECount, FEDimensionSize;
    infile >> FECount >> FEDimensionSize;

    std::cout << FECount << std::endl << FEDimensionSize << std::endl;

    m_DimensionSize = dimensionSize;
    m_FEDimensionSize = FEDimensionSize;

    for(int i = 0; i < FECount; i++) {
        int id = i;
        int materialId;
        infile >> materialId;

        std::vector<unsigned> NodesIds;
        for(int j = 0; j < FEDimensionSize; j++) {
            int tmpNodeId;
            infile >> tmpNodeId;
            NodesIds.push_back(tmpNodeId);

            if ( i == 0 ) {
                std::cout << tmpNodeId << "\n";
            }
        }

        cls_FE newFE(id, materialId, NodesIds);
        m_FE.push_back(newFE);
    }
}


int cls_FELoader::buildMatrix() {
    m_GlobalMatrix = new cls_RandBlockMatrixSymFace(4, 3);
    m_GlobalMatrix->fill();
}

cls_FELoader::~cls_FELoader() {
    delete m_GlobalMatrix;
}

cls_BlockMatrix *cls_FELoader::getMatrix() {
    return m_GlobalMatrix;
}
