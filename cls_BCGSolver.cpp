//
// Created by puhovity on 11.08.16.
//

#include <math.h>
#include "cls_BCGSolver.h"

cls_Vector cls_BCGSolver::solve(cls_CSLR& A, cls_Vector& b, double precision) {

    int threshold = 10000;

    // начальное приближение
    cls_Vector x(b.getSize());

    // начальная невязка
    cls_Vector r = b - A*x;
    cls_Vector p = r;

    int i = 0;
    while( i < threshold ) {

        cls_Vector Ap = A * p;
        double alpha = (r * r) / (Ap * p);
        x = x + p * alpha;
        cls_Vector prev_r = r;
        r = r - Ap * alpha;

        double beta = (r*r) / (prev_r * prev_r);
        if( sqrt(r*r) < precision ) {
            return x;
        }
        p = r + p*beta;

    }

    // возвращаем нулевой вектор в случае ошибки
    return x;

}
