//
// Created by puhovity on 02.08.16.
//

#ifndef MYREP_CLS_CSLR_H
#define MYREP_CLS_CSLR_H

#include "cls_BlockMatrix.h"
#include "cls_Vector.h"

class cls_CSLR {
protected:
    float* m_adiag;
    float* m_altr;
    float* m_autr;

    int* m_iptr;
    int* m_jptr;

    // количество элементов в m_altr
    int m_elemsCount;

    // размер стороны матрицы, т.е. количество элементов в m_adiag
    int m_matrixSize;

public:
    cls_CSLR(cls_BlockMatrix* inputMatrix);
    ~cls_CSLR();

    cls_Vector operator*(cls_Vector& vec);

};


#endif //MYREP_CLS_CSLR_H
