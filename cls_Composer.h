//
// Created by puhovity on 25.07.16.
//

#ifndef MYREP_CLS_COMPOSER_H
#define MYREP_CLS_COMPOSER_H

#include <map>
#include <vector>

class cls_Composer {
private:
    int m_size;
    std::vector<std::map<int, float>> struc;

public:
    cls_Composer();
    cls_Composer(int size);
    int setSize(int size);
    int setValue(int row, int col, float val);
    int getValue(int row, int col);
    bool isset(int row, int col);

    int getStoredElementsCount();
    int getLTStoredElementsCount();
    std::vector<std::map<int, float>>& getStruc();
};


#endif //MYREP_CLS_COMPOSER_H
