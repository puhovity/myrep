//
// Created by puhovity on 28.07.16.
//

#ifndef MYREP_LIB_PRINT_H
#define MYREP_LIB_PRINT_H

#include "cls_BlockMatrix.h"
#include "cls_SingleBlock.h"

void printBlockMatrix(cls_BlockMatrix& bMatrix) {
    int blocksCount = bMatrix.getBlocksCount();
    int blockSize = bMatrix.getBlockSize();

    for( int i = 0; i < blocksCount * blockSize; i++ ) {
        for( int j = 0; j < blocksCount * blockSize; j++ ) {
            float val = bMatrix.getValue(i, j);
            printf("%8.1f ", val);
        }
        printf("\n");
    }
}

void printSingleBlock(cls_SingleBlock& sBlock) {
    int size = sBlock.getSize();
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            printf("%f ", sBlock.getValue(i, j));
        }
        printf("\n");
    }
}

void printVector(cls_Vector vec) {
    int size = vec.getSize();
    printf("(");
    for(int i = 0; i < size; i++) {
        printf("%.32f ", vec.getValue(i));
    }
    printf(")\n");
}

#endif //MYREP_LIB_PRINT_H
