//
// Created by puhovity on 16.08.16.
//

#ifndef MYREP_FELOADER_H
#define MYREP_FELOADER_H

#include <string>
#include <vector>
#include "cls_Node.h"
#include "cls_FE.h"
#include "cls_BlockMatrix.h"

class cls_FELoader {
private:
    std::vector<cls_Node> m_Nodes;
    std::vector<cls_FE> m_FE;

    int m_DimensionSize;
    int m_FEDimensionSize;
    int m_NodesCount;

    cls_BlockMatrix* m_GlobalMatrix;

public:
    int loadFromFile(std::string path);
    int buildMatrix();

    cls_BlockMatrix* getMatrix();

    ~cls_FELoader();
};


#endif //MYREP_FELOADER_H
