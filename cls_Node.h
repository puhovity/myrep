//
// Created by puhovity on 10.08.16.
//

#ifndef MYREP_CLS_NODE_H
#define MYREP_CLS_NODE_H
#include <vector>

class cls_Node {
private:
    int m_Id;
    std::vector<double> m_Coords;

public:
    cls_Node();
    cls_Node(int id, double x, double y, double z);
    ~cls_Node();
};


#endif //MYREP_CLS_NODE_H
