//
// Created by puhovity on 10.08.16.
//

#include "cls_FE.h"

cls_FE::cls_FE() {
    m_Id = 0;
}

cls_FE::cls_FE(int id, int matId, std::vector<unsigned> nodesIds) {
    m_MatId = matId;
    m_Id = id;
    m_NodesIds = nodesIds;
}

cls_FE::NodesIdsContainer &cls_FE::getNodesIds() {
    return m_NodesIds;
}
