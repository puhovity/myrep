//
// Created by puhovity on 11.08.16.
//

#ifndef MYREP_CLS_BCGSOLVER_H
#define MYREP_CLS_BCGSOLVER_H

#include "cls_CSLR.h"

// решатель СЛАУ методом сопряженных градиентов
class cls_BCGSolver {

public:
    cls_Vector solve(cls_CSLR& A, cls_Vector& b, double precision);

};


#endif //MYREP_CLS_BCGSOLVER_H
