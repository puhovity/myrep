//
// Created by puhovity on 10.08.16.
//

#ifndef MYREP_CLS_SINGLEBLOCK_H
#define MYREP_CLS_SINGLEBLOCK_H

#include <vector>

class cls_SingleBlock {
private:
    int m_size;
    double* m_Data;

    // дополнительная информация
    std::pair<int, int> m_NodesIds;
public:
    cls_SingleBlock();
    cls_SingleBlock(int size);
    ~cls_SingleBlock();

    int getSize();
    void setNodesIds(std::pair<unsigned, unsigned> &nodesIds);
    void setValue(double value, int i, int j);
    double getValue(int i, int j);
    std::pair<unsigned, unsigned> getNodesIds();

};


#endif //MYREP_CLS_SINGLEBLOCK_H
