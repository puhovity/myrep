//
// Created by puhovity on 10.08.16.
//

#include "cls_SingleBlock.h"

cls_SingleBlock::cls_SingleBlock() {
    m_size = 3;
    m_Data = new double[m_size * m_size];
}


cls_SingleBlock::cls_SingleBlock(int size) {
    m_size = size;
    m_Data = new double[m_size * m_size];
}


cls_SingleBlock::~cls_SingleBlock() {
    delete[] m_Data;
}


int cls_SingleBlock::getSize() {
    return m_size;
}

std::pair<unsigned, unsigned> cls_SingleBlock::getNodesIds() {
    return m_NodesIds;
}

void cls_SingleBlock::setNodesIds(std::pair<unsigned, unsigned> &nodesIds) {
    m_NodesIds = nodesIds;
}


void cls_SingleBlock::setValue(double value, int i, int j) {
    m_Data[i*m_size + j] = value;
}


double cls_SingleBlock::getValue(int i, int j) {
    return m_Data[i*m_size + j];
}
