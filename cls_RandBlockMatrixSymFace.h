//
// Created by puhovity on 31.07.16.
//

#ifndef MYREP_CLS_RANDBLOCKMATRIXSYMFACE_H
#define MYREP_CLS_RANDBLOCKMATRIXSYMFACE_H

#include "cls_BlockMatrix.h"
class cls_RandBlockMatrixSymFace : public cls_BlockMatrix {
public:
    cls_RandBlockMatrixSymFace(int blocksCount, int blockSize);
    int fill();
    float randomVal();
};


#endif //MYREP_CLS_RANDBLOCKMATRIXSYMFACE_H
