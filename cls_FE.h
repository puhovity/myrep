//
// Created by puhovity on 10.08.16.
//

#ifndef MYREP_CLS_FE_H
#define MYREP_CLS_FE_H
#include <vector>

class cls_FE {
public:
    typedef std::vector<unsigned> NodesIdsContainer;
private:
    int m_Id;
    NodesIdsContainer m_NodesIds;
    unsigned m_MatId;
public:

    cls_FE();
    cls_FE(int id, int matId, std::vector<unsigned> nodesIds);
    NodesIdsContainer& getNodesIds();
};


#endif //MYREP_CLS_FE_H
