//
// Created by puhovity on 31.07.16.
//

#include "cls_RandBlockMatrixSymFace.h"

cls_RandBlockMatrixSymFace::cls_RandBlockMatrixSymFace(int blocksCount, int blockSize) {
    m_blocksCount = blocksCount;
    m_blockSize = blockSize;
    m_Composer.setSize(m_blocksCount * m_blockSize);
}

int cls_RandBlockMatrixSymFace::fill() {
    for(int i = 0; i < m_blockSize * m_blocksCount; i++) {
        for(int j = 0; j < m_blockSize * m_blocksCount; j++) {
            int probability_rand = rand() % 100;
            // заполнение только в 30% случаев
            if(probability_rand < 30) {
                float rand_val1 = randomVal();
                float rand_val2 = rand_val1;

                setValue(i, j, rand_val1);
                setValue(j, i, rand_val2);
            }
        }

        float rand_val = randomVal();
        setValue(i, i, rand_val);
    }
}

float cls_RandBlockMatrixSymFace::randomVal() {
    return rand() % 10 + 1;
}